﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class NewBehaviourScript : NetworkBehaviour {
	
	public float walkSpeed = 0.08f;
	public float runSpeed = 0.2f;

	public float forceConst = 2.0f;
	Rigidbody selfRigidbody;
	[SerializeField]
	Animator animator;
	// Use this for initialization
	void Start () {

		selfRigidbody = GetComponent<Rigidbody>();
		animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		bool running = false;
		int moving = 0;
		
		if(Input.GetKey(KeyCode.W))
			moving=1;

		if(Input.GetKey(KeyCode.LeftShift)&&Input.GetKey(KeyCode.W))
			{
				running = true;
				moving = 1;
			}

		float targetSpeed = ((running) ? runSpeed : walkSpeed);
		if(moving==1)
		transform.Translate(transform.forward * targetSpeed, Space.World);
		
		if(Input.GetKey(KeyCode.D))
			transform.Translate(transform.right * walkSpeed, Space.World);
		if(Input.GetKey(KeyCode.A))
			transform.Translate(-transform.right * walkSpeed, Space.World);
		if(Input.GetKey(KeyCode.S))
			transform.Translate(-transform.forward * walkSpeed, Space.World);
		if(Input.GetKeyDown(KeyCode.Space))
			selfRigidbody.AddForce(0, forceConst, 0, ForceMode.Impulse);

		float animationSpeedPercent = ((running) ? 1 : .5f) * moving;
		animator.SetFloat ("speedPercent", animationSpeedPercent, 0.1f, Time.deltaTime);
		Debug.Log(animationSpeedPercent+"");
	}
}
