﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Shoot : NetworkBehaviour {
	[SerializeField]
	private Camera Cam;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Mouse0))
		{	
			shoot();
		}
	}
	[Client]
	void shoot()
	{
		RaycastHit Hit;
		if(Physics.Raycast(Cam.transform.position,Cam.transform.forward,out Hit,10.0f))
		{
			CmdSendToServer(Hit.transform.name);
			//SendMessages();			
		}

	}
	// void SendMessages()
	// {
	// 	if(isServer)
	// 		SendToClient();
	// 	else
	// 		SendToServer();
		
	// }
	[Command]
	void CmdSendToServer(string name)
	{
		Debug.Log("We hit "+ name);	
		// GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
		// cube.transform.position=Cam.transform.position;
	}
}
