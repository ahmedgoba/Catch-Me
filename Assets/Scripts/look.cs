﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class look : MonoBehaviour {

	public float mouseSensitivity = 3.0f;

	private float rotY = 0.0f; // rotation around the up/y axis
	private float rotX = 0.0f; // rotation around the right/x axis

	void Start ()
	{
		Vector3 rot = transform.localRotation.eulerAngles;
		rotY = rot.y;
		rotX = rot.x;
	}

	void Update ()
	{
		float mouseX = Input.GetAxis("Mouse X");
		float mouseY = Input.GetAxis("Mouse Y");

		rotY += mouseX * mouseSensitivity;
		rotX += -mouseY * mouseSensitivity;
		if(rotX>90)
			rotX=90;
		else if(rotX<-90)
			rotX=-90;

		Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0.0f);		
		transform.rotation = localRotation;
		
		GameObject Player = this.transform.parent.gameObject;
		Player.transform.Rotate(0.0f, mouseX*mouseSensitivity, 0.0f);
		
	}
}
