﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerSetup : NetworkBehaviour {

	[SerializeField] 
	Behaviour[] ComponentsToDisable;
	// Use this for initialization
	void Start () {
		if(!isLocalPlayer)
		{
			for(int x=0;x<ComponentsToDisable.Length;x++)
			{
				ComponentsToDisable[x].enabled=false;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
